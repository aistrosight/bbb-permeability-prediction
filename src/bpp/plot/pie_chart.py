#!/usr/bin/env python3

'''
Plot pie charts with matplotlib.
'''

import pathlib
from collections import OrderedDict

from bpp.plot.subplots import SquarishSubplots


def plot_pie_chart(ax, labels, counts, title=None):  # pylint: disable=invalid-name
    '''
    Plot a pie chart on the given axis.

    Args:
        ax:
            The Matplotlib axis on which to plot the pie chart.

        labels:
            The string labels associated with the counts.

        counts:
            The counts to plot.

        title:
            An optional title for the plot.
    '''
    total = sum(counts)
    labels = [
        f'{(100. * count / total):0.1f}% {label}\n{count:d}'
        for (label, count) in zip(labels, counts)
    ]

    ax.pie(
        counts,
        labels=labels,
        labeldistance=0.4,
        startangle=90,
        textprops={'color': 'w'}
    )
    if title is not None:
        ax.set_title(title)


def plot_pie_charts(datasets, values=None, path=None, **kwargs):
    '''
    Plot multiple pie charts in one image.

    Args:
        datasets:
            A list of 2-tuples containing a title and a data series.

        values:
            The values to count in the pie chart. If None, then all values
            across all datasets will be counted for each dataset.

        path:
            The output image path. If not given, then this function will return
            the Matplotlib figure object.

        **kwargs:
            All other keyword arguments passed through to SquarishSubplots.

    Returns:
        The Matplotlib figure object if no path was given, else the path.
    '''
    datasets = tuple(datasets)

    # Default to an equal aspect ratio for subplots.
    kwargs.setdefault(
        'subplots_kwargs', {}
    ).setdefault(
        'subplot_kw', {}
    ).setdefault(
        'aspect', 'equal'
    )

    # Default to a subplot size of (6, 6).
    kwargs.setdefault('subplot_size', (6, 6))

    subplots = SquarishSubplots(len(datasets), **kwargs)

    if values is None:
        values = set()
        for _, data in datasets:
            values.update(data.unique())
        values = sorted(values)
    else:
        # Preserve the order of the given values while ensuring that each value
        # is unique.
        values = list(OrderedDict((val, None) for val in values))

    for i, (title, data) in enumerate(datasets):
        counts = [(data == val).sum() for val in values]
        plot_pie_chart(subplots[i], values, counts, title=title)

    if not path:
        return subplots.fig

    path = pathlib.Path(path).resolve()
    path.parent.mkdir(parents=True, exist_ok=True)
    subplots.fig.savefig(path, bbox_inches='tight', transparent=False)

    return path
