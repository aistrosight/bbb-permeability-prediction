#!/usr/bin/env python3

'''
Subplot organization.
'''

import math

import matplotlib.pyplot as plt


class SquarishSubplots():
    '''
    A simple manager for organizing subplots. The number of rows and columns
    will be set to make the resulting image as close to a square as as possible.
    '''

    def __init__(self, n_subplots, subplot_size=(6, 6), subplots_kwargs=None):
        '''
        Args:
            n_subplots:
                The number of subplots.

            subplot_size:
                The width and height of each subplot as a 2-tuple.

            subplot_kwargs:
                A dict of keywork arguments to pass to plt.subplots.
        '''
        self.n_subplots = n_subplots
        width, height = self.subplot_size = subplot_size

        self.n_cols = math.ceil(math.sqrt(height * n_subplots / width))
        self.n_rows = math.ceil(n_subplots / self.n_cols)

        if subplots_kwargs is None:
            subplots_kwargs = {}
        subplots_kwargs.setdefault('squeeze', False)
        subplots_kwargs['nrows'] = self.n_rows
        subplots_kwargs['ncols'] = self.n_cols
        subplots_kwargs['figsize'] = (width * self.n_cols, height * self.n_rows)
        self.fig, self.axes = plt.subplots(**subplots_kwargs)

        # Delete unused axes on the last row.
        last_col = n_subplots % self.n_cols
        if last_col:
            axes = self.axes[self.n_rows - 1]
            for col in range(last_col, self.n_cols):
                self.fig.delaxes(axes[col])

    def _get_ax(self, index):
        '''
        Get the axes of the given subplot.

        Args:
            index:
                The subplot index.

        Returns:
            The corresponding axes.

        Raises:
            IndexError:
                The index is out of range.
        '''
        if index < 0 or index >= self.n_subplots:
            raise IndexError('subplot index out of range')
        row, col = divmod(index, self.n_cols)
        return self.axes[row][col]

    def __getitem__(self, items):
        if isinstance(items, int):
            if items < 0:
                items = self.n_subplots + items
            return self._get_ax(items)
        if isinstance(items, slice):
            return [self._get_ax(i) for i in range(self.n_subplots)[items]]
        raise ValueError(
            f'{self.__class__.__name__}.__getitem__ received '
            f'an unsupported argument type: {type(items)}'
        )

    def __iter__(self):
        for i in range(self.n_subplots):
            yield self._get_ax(i)
