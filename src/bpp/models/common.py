#!/usr/bin/env python3
'''
Common base class for other models.
'''

import logging

import matplotlib.pyplot as plt

from sklearn.metrics import ConfusionMatrixDisplay

from molpred.experiment import OperationMode
from molpred.model.base import ModelBase

from bpp.plot.pie_chart import plot_pie_charts


LOGGER = logging.getLogger(__name__)


class BinaryClassifierBase(ModelBase):  # pylint: disable=abstract-method
    '''
    Base class for binary classifiers.
    '''

    def visualize_data(self):
        experiment = self.experiment
        mode = experiment.mode

        if mode is OperationMode.TRAIN:
            titles_and_names = [
                ('Input Data', 'feature_data'),
                ('Train Data', 'train_data'),
                ('Test Data', 'test_data')
            ]
        elif mode is OperationMode.TEST:
            titles_and_names = [
                ('Test Data', 'test_data')
            ]
        else:
            titles_and_names = []

        pie_chart_datasets = []
        for title, name in titles_and_names:
            targets = experiment.load_features_and_targets(name).targets.iloc[:, 0]
            pie_chart_datasets.append((title, targets))

        pie_chart_fig = plot_pie_charts(pie_chart_datasets)
        experiment.log_figure(pie_chart_fig, f'{self.image_subdirectory}/input_data_pie_charts.png')

    def visualize_prediction_metrics(self, data, target):
        prediction = self.predict(data)
        fig, ax = plt.subplots(subplot_kw={'aspect': 'equal'})  # pylint: disable=invalid-name
        ConfusionMatrixDisplay.from_predictions(target, prediction, ax=ax)
        self.experiment.log_figure(fig, f'{self.image_subdirectory}/confusion_matrix.png')
