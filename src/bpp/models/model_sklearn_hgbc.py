#!/usr/bin/env python3
'''
Sklearn HistGradBoost model.
'''

import logging
import pickle
import pathlib

from sklearn.compose import make_column_transformer
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

import mlflow.sklearn
import pandas as pd

from molpred.decorators import caching_method

from bpp.models.common import BinaryClassifierBase


LOGGER = logging.getLogger(__name__)


class SklearnHistGradientBoostingClassifierModel(BinaryClassifierBase):
    '''
    Model based on Sklearn's HistGradBoost classifier.
    '''

    def __init__(self, *args, **kwargs):
        '''
        Recognized model parameters (experiment.params.model.params):
            hgbc_kwargs:
                A dict of keyword arguments passed through to
                HistGradientBoostingClassifier.

            cv_splits:
                The number of cross-validations splits to use.

            n_jobs:
                The number of jobs to use during cross-validation.
        '''
        mlflow.sklearn.autolog()
        super().__init__(*args, **kwargs)

    def initialize(self):
        model = HistGradientBoostingClassifier(
            **self.param_get_container('hgbc_kwargs', default={})
        )
        num_feats = self.experiment.numeric_feature_names
        if num_feats:
            preprocessor = make_column_transformer(
                (StandardScaler(), sorted(num_feats)),
                remainder='passthrough'
            )
            model = make_pipeline(preprocessor, model)
        self.model = model

    def load(self, path):
        path = pathlib.Path(path).resolve()
        LOGGER.info(
            'Loading %s model from %s',
            self.__class__.__name__,
            path
        )
        with path.open('rb') as handle:
            self.model = pickle.load(handle)

    def save(self, path):
        path = pathlib.Path(path).resolve()
        LOGGER.info(
            'Saving %s model to %s',
            self.__class__.__name__,
            path
        )
        with path.open('wb') as handle:
            pickle.dump(self.model, handle)

    def _cross_validate(self, data, targets):
        '''
        Cross-validate the model on the train data.
        '''
        LOGGER.info('Running cross-validation')
        self.model.fit(data, targets)

        scoring = 'balanced_accuracy'
        cv_splits = self.param_get('cv_splits', default=5)
        n_jobs = self.param_get('n_jobs', default=1)

        cv_results = pd.DataFrame(
            cross_validate(
                self.model,
                data,
                targets,
                scoring=scoring,
                cv=StratifiedKFold(n_splits=cv_splits),
                n_jobs=n_jobs,
                return_train_score=True,
                return_estimator=False,
                verbose=False
            )
        )
        cv_results_path = pathlib.Path('cv_results.csv')
        cv_results.to_csv(cv_results_path, index=False)
        exp = self.experiment
        exp.log_artifact(cv_results_path, artifact_path='data')

        for field, name in (
            ('train_score', 'train'),
            ('test_score', 'validation')
        ):
            exp.log_metric(f'cv_{scoring}_score_{name}_data-mean', cv_results[field].mean())
            for cv_i, score in enumerate(cv_results[field]):
                exp.log_metric(f'cv_{scoring}_score_{name}_data-cv_{cv_i:d}', score)

        return cv_results

    def train(self, data, targets):
        targets = targets.iloc[:, 0]
        mask = targets.notna()
        dropped = targets.size - mask.sum()
        if dropped:
            LOGGER.warning('Dropped %d rows from train data due to missing target class.', dropped)
        data = data[mask]
        targets = targets[mask]

        cv_results = self._cross_validate(data, targets)

        LOGGER.info('Refitting model to full train data set')
        self.model.fit(data, targets)

        return cv_results['test_score'].mean()

    @caching_method
    def predict(self, data):
        return pd.Series(self.model.predict(data), name='Prediction')

    @caching_method
    def predict_proba(self, data):
        return pd.Series(self.model.predict_proba(data), name='Prediction Probabilities')


SklearnHistGradientBoostingClassifierModel.register()
