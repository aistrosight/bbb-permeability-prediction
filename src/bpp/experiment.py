#!/usr/bin/env python3
'''
Experiment subclass for Hydronaut.
'''

import logging
import pathlib

from hydra.utils import to_absolute_path
from sklearn import metrics

from molpred.data.inchi_loader import InChiLoader
from molpred.experiment import MolPredExperiment
from molpred.model.base import import_models
from molpred.model.scoring import register_scorer

from bpp.data import prepare_b3db_data, bbb_class_to_bool


LOGGER = logging.getLogger(__name__)


# ------------ Register transformers for handling the input data ------------- #
InChiLoader.register_transformer('bbb_class_to_bool', bbb_class_to_bool)

# -------------------- Import custom ModelBase subclasses -------------------- #
import_models((pathlib.Path(__file__).resolve().parent / 'models').glob('model_*.py'))


# ----------------------------- Register scorers ----------------------------- #
def register_scorers():
    '''
    Register some scorers.
    '''
    for name, func, kwargs in (
        ('accuracy', metrics.accuracy_score, {}),
        ('balanced_accuracy', metrics.balanced_accuracy_score, {}),
        ('recall', metrics.recall_score, {'pos_label': True}),
        ('specificity', metrics.recall_score, {'pos_label': False}),
        ('precision', metrics.precision_score, {'pos_label': True})
    ):
        scorer = metrics.make_scorer(func, **kwargs)
        register_scorer(name, scorer)


register_scorers()


# -------------------------------- Experiment -------------------------------- #
class BBBExperiment(MolPredExperiment):
    '''
    BBB permeability prediction Experiment subclass for Hydronaut.
    '''
    def _maybe_prepare_b3db(self):
        '''
        Prepare B3DB data if required.
        '''
        input_params = self.config.experiment.params.input

        # The input data from B3DB.
        if input_params.get('B3DB', False):
            prepare_b3db_data(
                to_absolute_path(input_params.path),
                n_samples_per_class=input_params.get('n_samples_per_class')
            )

    def setup(self):
        self._maybe_prepare_b3db()
        super().setup()
