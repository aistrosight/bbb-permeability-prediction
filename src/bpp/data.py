#!/usr/bin/env python3

'''
Input data preparation.
'''

import logging
import pathlib

import pandas as pd
import rdkit.Chem.inchi as rci
from hydra.utils import to_absolute_path

from b3db import Loader
from simple_file_lock import FileLock


LOGGER = logging.getLogger(__name__)


BBB_COLUMN = 'BBB+/BBB-'
BBB_PLUS = 'BBB+'
BBB_MINUS = 'BBB-'
INCHI_COLUMN = 'InChi'
INCHI_KEY_COLUMN = 'InChi key'


def bbb_class_to_bool(bbb_cls):
    '''
    Convert a BBB class (BBB_PLUS or BBB_MINUS) to a boolean value.

    Args:
        bbb_cls:
            The input class.

    Returns:
        True if BBB_PLUS, False if BBB_MINUS, None otherwise.
    '''
    if isinstance(bbb_cls, str):
        bbb_cls = bbb_cls.strip().upper()
    if bbb_cls == BBB_PLUS:
        return True
    if bbb_cls == BBB_MINUS:
        return False
    return None


def bool_to_bbb_class(bbb_bool):
    '''
    The opposite of bbb_class_to_bool().

    Args:
        bbb_bool:
            The input boolean, or None.

    Returns:
        BBB_PLUS if True, BBB_MINUS if False, None otherwise.
    '''
    if bbb_bool is None:
        return None
    return BBB_PLUS if bbb_bool else BBB_MINUS


def prepare_b3db_data(path, n_samples_per_class=None, inchi_column='Inchi'):
    '''
    Save the B3DB data to the given path.

    Args:
        path:
            The output path.

        n_samples_per_class:
            A optional maximum number of samples of each class. This is only for
            testing the workflow with a limited number of samples.

        inchi_column:
            The name of the column with the InChi data.

    Returns:
        The output path as a resolved pathlib.Path object.
    '''
    path = pathlib.Path(to_absolute_path(path)).resolve()

    with FileLock(path):
        if not path.exists():
            LOGGER.debug('Saving B3DB data to %s', path)
            loader = Loader()
            data = loader.get_classification_data()

            # Filter based on known classes.
            data = data[data[BBB_COLUMN].isin((BBB_PLUS, BBB_MINUS))]

            if n_samples_per_class is not None and n_samples_per_class > 0:
                plus = data[data[BBB_COLUMN] == BBB_PLUS]
                minus = data[data[BBB_COLUMN] == BBB_MINUS]
                data = pd.concat([plus.head(n_samples_per_class), minus.head(n_samples_per_class)])

            data.rename(
                columns={inchi_column: INCHI_COLUMN},
                inplace=True
            )
            data[INCHI_KEY_COLUMN] = [
                rci.InchiToInchiKey(inchi)
                for inchi in data[INCHI_COLUMN]
            ]

            path.parent.mkdir(parents=True, exist_ok=True)
            data.to_csv(path, index=False)
    return path
