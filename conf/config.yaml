defaults:
  - experiment/hydronaut_experiment
  - feature_set: qed_desc_rdkfp_2048
  - override hydra/sweeper: optuna
  - override hydra/sweeper/sampler: tpe
  # - override hydra/launcher: joblib
  - _self_

hydra:
  mode: MULTIRUN
  job:
    chdir: true
  sweeper:
    direction: maximize
    study_name: ${experiment.name}-study
    n_trials: 200
    # n_jobs: ${n_cpu:${experiment.params.n_jobs}}
    n_jobs: 1
    sampler:
      seed: 123
    params:
      ++experiment.params.model.params.hgbc_kwargs.max_leaf_nodes: range(2, 50)
      ++experiment.params.model.params.hgbc_kwargs.learning_rate: interval(0.05, 0.2)
      ++experiment.params.model.params.hgbc_kwargs.max_iter: range(30, 60)

experiment:
  name: "BBB Permeability Prediction: feature set ${feature_set.name}: ${experiment.params.mode}"
  description: Train a model to predict BBB permeability.
  exp_class: bpp.experiment:BBBExperiment
  params:
    # Molpred Operation mode.
    mode: train
    # Run ID for testing and prediction.
    mlflow_run_id: null

    # Directory for intermediate data files.
    data_dir: data

    # The number of jobs to use for parallel calculations such as feature
    # calculations.
    n_jobs: ${n_cpu:}

    # Input data.
    input:
      path: data/b3db.csv
      # If true, retrieve data from B3DB.
      B3DB: true
      # The number of samples per class to use, to reduce the data when testing
      # the code.
      # n_samples_per_class: 20

    plot_features: true

    chemfeat_features: ${feature_set.chemfeat_features}

    # Input data loader configuration.
    InChiLoader:
      path: ${experiment.params.input.path}
      inchi_column: InChi
      column_specifier:
        # Set the categorical target column.
        cat_targets:
          - 'BBB+/BBB-'
      transformers:
        - ['BBB+/BBB-', bbb_class_to_bool]

    # Model parameters.
    model:
      # Registered model class name.
      name: SklearnHistGradientBoostingClassifierModel

      # The path for reloading a trained model for testing and prediction.
      path: null

      # Scorers for logging metrics.
      scorers:
        - accuracy
        - balanced_accuracy
        - recall
        - specificity
        - precision

      # Model-specific parameters.
      params:
        hgbc_kwargs:
          verbose: false
          max_leaf_nodes: 8
          learning_rate: 1
          max_iter: 40
        cv_splits: 5
        n_jobs: ${experiment.params.n_jobs}

    # Objective values
    objective_values:
      - train_score

    # Configure the train-test split.
    train_test_split:
      train_size: 0.75
      balanced: True

  python:
    paths:
        - src
