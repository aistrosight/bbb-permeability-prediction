---
title: README
author: Jan-Michael Rye
---

# Synopsis

Create a machine-learning model to predict the blood-brain barrier permeability of chemical compounds. The model is trained and tested using the [Blood-Brain Barrier Database](https://github.com/theochem/B3DB) with features calculated with the open-source [RDKit](https://rdkit.org/) cheminformatics software package.

This project uses the [Molpred](https://gitlab.inria.fr/jrye/molpred) framework which is based on [Hydronaut](https://gitlab.inria.fr/jrye/hydronaut) and [Chemfeat](https://gitlab.inria.fr/jrye/chemfeat). Hydronaut provides a framework for hyperparameter management, exploration and optimization, as well as systematic tracking of all experiments, including their parameters, metrics and artifacts. Chemfeat provides an easy way to configure and generate feature vectors by arbitrarily combining a large number of different chemical feature sets.

# Links

* [Chemfeat](https://gitlab.inria.fr/jrye/chemfeat)
* [Hydronaut](https://gitlab.inria.fr/jrye/hydronaut)
* [Molpred](https://gitlab.inria.fr/jrye/molpred)

# Cloning

This repository contains submodules. Recursively clone everything with `git clone --recursive ...` or run `git submodule update --init --recursive` after cloning the repository normally.

## Submodules

* [B3DB](https://github.com/theochem/B3DB.git) - the Blood-Brain Barrier Database.
* [b3db](https://gitlab.inria.fr/aistrosight/b3db) - load the B3DB data.
* [utility-scripts](https://gitlab.inria.fr/jrye/utility-scripts) - required for this repository's utility scripts.

# Usage

~~~sh
# Prepare the virtual environment.
./scripts/bpp-prepare_venv.sh

# Active the virtual environment.
source venv/bin/active

# Set up other environment variables.
source env.sh

# Run the experiment. Depending on the parameter sweep space and optimizer stopping conditions, this may take a while.
hydronaut-run 

# Launch the MLflow web interface and then open the URL with a web browser.
mlflow ui
~~~

The experiment is configured via the [config.yaml](conf/config.yaml) file, which will use multiple runs to attempt to optimize various hyperparameters by default using [Hydra's](https://hydra.cc/docs/intro/) [Optuna Sweeper pluging](https://hydra.cc/docs/plugins/optuna_sweeper/).

For an explanation of the different feature sets used, consult [Chemfeat's documentation of supported feature sets](https://jrye.gitlabpages.inria.fr/chemfeat/gen_features.html) and [how to configure them](https://jrye.gitlabpages.inria.fr/chemfeat/gen_feature_set_configuration.html).
