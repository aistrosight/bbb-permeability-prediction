#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
source "${SELF%/*/*}/env.sh"

cmd=(
  "$BBB_PERM_PRED_DIR/submodules/utility-scripts/scripts/pip-install_in_venv.sh"
  "$@"
  "$BBB_PERM_PRED_DIR"
  "$BBB_PERM_PRED_DIR/submodules"/b3db
)
echo "${cmd[*]@Q}"
"${cmd[@]}"
