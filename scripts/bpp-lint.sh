#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
source "${SELF%/*/*}/env.sh"

"$BBB_PERM_PRED_DIR/submodules/utility-scripts/scripts/pylint.sh" \
  "$BBB_PERM_PRED_DIR/src"
