#!/usr/bin/env bash

# Get the absolute path to the project's directory.
BBB_PERM_PRED_ENV_FILE=$(readlink -f "${BASH_SOURCE[0]}")
BBB_PERM_PRED_DIR=${BBB_PERM_PRED_ENV_FILE%/*}
BBB_PERM_PRED_DEFAULT_VENV_DIR=$BBB_PERM_PRED_DIR/venv

# Set the path to the B3DB data directory.
export B3DB_DIR=$(readlink -f "$BBB_PERM_PRED_DIR/submodules/B3DB/B3DB")

source "$BBB_PERM_PRED_DIR/submodules/utility-scripts/sh/prepend_to_paths.sh"
prepend_to_PATH "$BBB_PERM_PRED_DIR/submodules/utility-scripts/scripts"
prepend_to_PYTHONPATH "$BBB_PERM_PRED_DIR/src"
